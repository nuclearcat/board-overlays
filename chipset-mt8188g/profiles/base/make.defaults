# Copyright 2022 The ChromiumOS Authors
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

CHROMEOS_KERNEL_SPLITCONFIG="chromiumos-mediatek"
CHROMEOS_KERNEL_ARCH="arm64"

# MT8188G has 6 Cortex-A55 and 2 Cortex-A78.
# Note that the compiler may or may not support tuning for our big.LITTLE
# combination, so for simplicity we tune the code against the little cores,
# which is not perfect but should give us good enough tuning granularity.
# TODO(b/244126739): Use cortex-a78.cortex-a55 when the compiler is upgraded
# and supports that.
BOARD_COMPILER_FLAGS="-march=armv8-a+crc+crypto -mtune=cortex-a55 -ftree-vectorize"

# TODO(gbiv): Porting -mtune may be interesting, but clang seems to currently
# entirely ignore this flag for ARM (notably: not aarch64) builds.
BOARD_RUSTFLAGS="-Ctarget-feature=+armv8-a,+crc"

# Initial value just for style purposes.
USE=""

# Use hardware FPU instead of software
USE="${USE} hardfp"
# Support for neon-vfpv3
USE="${USE} neon"

# For the device tree installation
USE="${USE} device_tree"

# TODO (b/245739330): use kernel-upstream for now since the repo for our target
# kernel is not ready yet. Update this when the target kernel repo is ready.
USE="${USE} kernel-upstream"

# Enables WebGL rendering
USE="${USE} drm_atomic"

# Set raster thread number=2
USE="${USE} big_little"

# Turn on the video cards this board uses.
VIDEO_CARDS="mediatek"

# Enable fscrypt v2 usage on 5.4+
USE="${USE} direncription_allow_v2"

# Enable HW codecs using V4L2 API.
USE="${USE} v4l2_codec"

# Enable GpuMemoryBuffers backed by dma-bufs.
USE="${USE} native_gpu_memory_buffers"

# Allows Chrome to start in the presence of multiple threads.
# We need this to boot to login screen with Mali GPU.
USE="${USE} gpu_sandbox_start_early"

# MT8188G minigbm
USE="${USE} minigbm_platform_mt8188g"
