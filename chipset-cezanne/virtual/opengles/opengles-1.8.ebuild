# Copyright 2020 The ChromiumOS Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="Virtual for OpenGLES implementations"

SLOT="0"
KEYWORDS="*"
IUSE=""

DEPEND="media-libs/mesa-amd"
RDEPEND="${DEPEND}"
